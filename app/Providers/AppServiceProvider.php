<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Livewire\GlamsWizardComponent;
use App\Http\Livewire\Steps\IntroStepComponent;
use App\Http\Livewire\Steps\MuseumStepComponent;
use App\Http\Livewire\Steps\SpokePersonStepComponent;
use App\Http\Livewire\Steps\ApplicationStepComponent;
use App\Http\Livewire\Steps\EditMuseumDataStepComponent;
use App\Http\Livewire\Steps\MuseumDataForWikipediaStepComponent;
use App\Http\Livewire\Steps\ImageAuthorizationStepComponent;
use App\Http\Livewire\Steps\ImagesUploadStepComponent;
use App\Http\Livewire\Steps\FinishStepComponent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\URL;
use Livewire\Livewire;

class AppServiceProvider extends ServiceProvider
{
    public function boot()
    {
        Model::unguard();

        Blade::component('layouts.layout', 'layout');
        Blade::component('components.button', 'button');

        Livewire::component('index-wizard', GlamsWizardComponent::class);
        Livewire::component('intro', IntroStepComponent::class);
        Livewire::component('museum', MuseumStepComponent::class);
        Livewire::component('spokeperson', SpokePersonStepComponent::class);
        Livewire::component('application', ApplicationStepComponent::class);
        Livewire::component('editmuseumdata', EditMuseumDataStepComponent::class);
        Livewire::component('museumdataforwikipedia', MuseumDataForWikipediaStepComponent::class);
        Livewire::component('imageauthorization', ImageAuthorizationStepComponent::class);
        Livewire::component('imagesupload', ImagesUploadStepComponent::class);
        Livewire::component('finish', FinishStepComponent::class);
    }
}
