<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\HtmlString;

class ApplicationNotification extends Notification
{
    use Queueable;

    /**
     * Data to show
     *
     * @var array $email
     */
    protected $email;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($email)
    {
        $this->email = $email;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
        ->subject($this->email['subject'])
        ->greeting(' ')->salutation(' ')
        ->attach(storage_path() . '/Modello_autorizzazione_e_open_access_policy_Empowering_Italian_GLAMs.docx')
        ->attach(storage_path() . '/Modello_autorizzazione_e_open_access_policy_Empowering_Italian_GLAMs.odt')
        ->attach(storage_path() . '/Modello_lettera_adesione.docx')
        ->attach(storage_path() . '/Modello_lettera_adesione.odt')
        ->line($this->email['body']);
    }
}
