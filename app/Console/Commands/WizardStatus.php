<?php

namespace App\Console\Commands;

use Notification;
use Carbon\Carbon;
use App\Models\MuseumData;
use App\Models\MuseiComuni;
use App\Models\Spokeperson;
use App\Notifications\EmailNotification;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\HtmlString;

class WizardStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:wizard';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send periodic email about Wizard status to museums';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $museumdata = MuseumData::query()
            ->select('*')->whereBetween(
                'updated_at',
                [Carbon::now()->subMonth(4), Carbon::now()]
                ->where('notification', 1)
            )->get()->toArray();

        foreach ($museumdata as &$item) {
            $email = $item['email'];
            if (empty($email)) {
                $dataSpokepeople = Spokeperson::query()
                ->distinct()->select('*')
                ->where('museumID', $item['museumID'])
                ->orWhere('museumID', '=', $item['id'])
                ->get()->toArray();
                $email = $dataSpokepeople[0]['email'];
            }

            if (!empty($email)) {
                $museum = $item['name'];
                $status = MuseumData::wizardFinished($item);

                if ($status['total'] === 6) {
                    continue;
                }

                echo "Sending email to " . $email . " about " . $item['name'] . "\n";
                $body = array();

                if ($item['museumID'] === 'Q') {
                    $museumquery = MuseiComuni::query()
                    ->distinct()->select('*')
                    ->where('wikidata', '=', $item['museumID'])
                    ->get()->toArray();
                    $museum = $museumquery['nome'];
                }

                $body[] = new HtmlString(__('messages.notification.1_nospokepeople', [
                    'museumname' => $museum,
                ]));

                $spokepeople = new Spokeperson();
                $dataSpokepeople = $spokepeople::query()
                ->distinct()->select('*')
                ->where('museumID', '=', $item['museumID'])
                ->orWhere('museumID', '=', $item['id'])
                ->get()->toArray();

                if (is_array($dataSpokepeople) && !empty($dataSpokepeople)) {
                    $spokepeopleString = $dataSpokepeople[0]['name'] . ' ' . $dataSpokepeople[0]['surname'];

                    $body[0] = new HtmlString(__('messages.notification.1', [
                        'museumname' => $museum,
                        'spokepeople' => $spokepeopleString,
                    ]));
                }

                $_museum = $item['museumID'];
                if (empty($_museum)) {
                    $_museum = $item['id'];
                }

                if ($status['spokeperson']) {
                    $body[] = '✓' . __('messages.finish_list.1');
                }

                if ($status['application']) {
                    $body[] = '✓' . __('messages.finish_list.2');
                }

                if ($status['editmuseumdata']) {
                    $body[] = '✓' . __('messages.finish_list.3');
                }

                if ($status['museumdataforwikipedia']) {
                    $body[] = '✓' . __('messages.finish_list.4');
                }

                if ($status['imageauthorization']) {
                    $body[] = '✓' . __('messages.finish_list.5');
                }

                if ($status['imagesupload']) {
                    $body[] = '✓' . __('messages.finish_list.6');
                }

                $body[] = new HtmlString(__('messages.notification.2', [
                    'resumelink' => URL::to('/') . '?step=resume&museum=' . $_museum,
                ]));
                $body[] = new HtmlString(__('messages.notification.3'));

                $project = [
                    'subject' => __('messages.notification.subject'),
                    'body' => $body
                ];

                Notification::route('mail', $email)->notify(new EmailNotification($project));
            }
        }

        return Command::SUCCESS;
    }
}
