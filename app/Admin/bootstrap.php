<?php

/**
 * Laravel-admin - admin builder based on Laravel.
 *
 * @author z-song <https://github.com/z-song>
 *
 * Bootstraper for Admin.
 *
 * Here you can remove builtin form field:
 * Encore\Admin\Form::forget(['map', 'editor']);
 *
 * Or extend custom form field:
 * Encore\Admin\Form::extend('php', PHPEditor::class);
 *
 * Or require js and css assets:
 * Admin::css('/packages/prettydocs/css/styles.css');
 * Admin::js('/packages/prettydocs/js/main.js');
 */

use App\Admin\Extensions\JSONLink;
use App\Admin\Extensions\TextLink;
use App\Admin\Extensions\Types;
use App\Admin\Extensions\SpokepeopleData;
use Encore\Admin\Form;

Form::extend('jsonlink', JSONLink::class);
Form::extend('types', Types::class);
Form::extend('textlink', TextLink::class);
Form::extend('spokepeopledata', SpokepeopleData::class);

Encore\Admin\Form::forget(['map', 'editor']);
