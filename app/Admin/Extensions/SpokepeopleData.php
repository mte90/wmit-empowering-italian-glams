<?php

namespace App\Admin\Extensions;

use Encore\Admin\Form\Field;
use App\Models\Spokeperson;

class SpokepeopleData extends Field
{
    protected $view = 'admin.spokepeopledata';

    public function render()
    {
        $spokepeople = new Spokeperson();
        $data = $spokepeople::query()
        ->distinct()->select('*')
        ->where('museumID', '=', $this->data['museumID'])
        ->orWhere('museumID', '=', $this->data['id'])
        ->get()->toArray();

        if (isset($data[0])) {
            $this->addVariables(['data' => $data[0]]);
        } else {
            $this->addVariables(['data' => array('name' => '', 'surname' => '', 'role' => '', 'email' => '', 'note' => '')]);
        }

        return parent::render();
    }
}
