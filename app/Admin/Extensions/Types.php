<?php

namespace App\Admin\Extensions;

use Encore\Admin\Form\Field;
use App\Models\MuseumData;

class Types extends Field
{
    protected $view = 'admin.types';

    public function render()
    {
        $types = json_decode($this->data[$this->id]);
        $list = '<ul>';
        $origTypes = new MuseumData();
        if (is_array($types)) {
            foreach ($types as $type) {
                $list .= '<li>' . $origTypes->museumType[$type] . '</li>';
            }
        }

        $list .= '</ul>';
        $this->addVariables(['list' => $list]);

        return parent::render();
    }
}
