<?php

namespace App\Admin\Extensions;

use Encore\Admin\Form\Field;

class TextLink extends Field
{
    protected $view = 'admin.textlink';

    public function render()
    {
        $link = $this->data[$this->id];
        $list = '<a href="' . url('storage/' . $link) . '" target="_blank">' . $link . '</a>';

        $this->addVariables(['link' => $list]);

        return parent::render();
    }
}
