<?php

namespace App\Admin\Extensions;

use Encore\Admin\Form\Field;

class JSONLink extends Field
{
    protected $view = 'admin.jsonlink';

    public function render()
    {
        $images = json_decode($this->data[$this->id]);
        $list = '';
        if (is_array($images)) {
            $list = '<ul>';

            foreach ($images as $image) {
                $list .= '<li><a href="' . url('storage/' . $image) . '" target="_blank">' . $image . '</a></li>';
            }

            $list .= '</ul>';

        }

        $this->addVariables(['list'=>$list]);

        return parent::render();
    }
}

