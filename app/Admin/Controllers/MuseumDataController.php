<?php

namespace App\Admin\Controllers;

use App\Models\MuseumData;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\Admin\Actions\View;
use App\Models\Spokeperson;

class MuseumDataController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'MuseumData';

    /**
     * Variable to cache the query for spokeperson on export
     *
     * @var array
     */
    protected $spokeperson_cache = array();

    /**
     * Variable to cache the whole steps status of every museum on export
     *
     * @var array
     */
    protected $wizard_cache = array();

    protected function spokeperson_cache($record)
    {
        if (isset($this->spokeperson_cache[$record['id']])) {
            return $this->spokeperson_cache[$record['id']];
        }
        $spokepeople = new Spokeperson();
        $data = $spokepeople::query()
            ->distinct()->select('*')
            ->where('museumID', '=', $record['museumID'])
            ->orWhere('museumID', '=', $record['id'])
            ->get()->toArray();

        if (!empty($data[0])) {
            $this->spokeperson_cache[$record['id']] = $data;
        }

        return $data;
    }

    protected function wizard_cache($museum, $step) {
        if (!isset($this->wizard_cache[$museum])) {
            $museumdata = MuseumData::query()
            ->distinct()->select('*')
            ->Where('id', '=', $museum)
            ->get()->toArray()[0];
            $this->wizard_cache[$museum] = MuseumData::wizardFinished($museumdata);
            $this->wizard_cache[$museum]['survey'] = $museumdata['survey'];
        }

        return $this->wizard_cache[$museum][$step];
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new MuseumData());

        $grid->column('museumID', __('messages.museum_data.museumWikidataID'));
        $grid->column('name', __('messages.museum_data.name'));
        $grid->column('address', __('messages.museum_data.address'));
        $grid->column('municipality', __('messages.museum_data.municipality'));
        $grid->column('owner', __('messages.museum_data.owner'));
        $grid->column('manager', __('messages.museum_data.manager'));
        $grid->column('website', __('messages.museum_data.website'))->link();
        // Workaround to add custom columns only on the xported file
        if (isset($_GET['_export_'])) {
            $grid->column('spokepeople', __('messages.museum_data.spokepeople'));
            $grid->column('role', __('messages.spokeperson_role'));
            $grid->column('email', __('messages.museum_data.email'));
            $grid->column('step1', __('messages.step') . ' 1');
            $grid->column('step2', __('messages.step') . ' 2');
            $grid->column('step3', __('messages.step') . ' 3');
            $grid->column('step3-1', __('messages.step') . ' 3.1');
            $grid->column('step4', __('messages.step') . ' 4');
            $grid->column('step5', __('messages.step') . ' 5');
            $grid->column('step6', __('messages.step') . ' 6');
            $grid->column('step7', __('messages.step') . ' 7');
        }

        $grid->disableCreateButton();

        $grid->actions(function ($actions) {
            $actions->disableView();
            $actions->add(new View);
        });

        $_this = $this;

        $grid->export(function ($export) use ($_this) {
            $export->except(['authorizationLetter', 'images', 'letter']);
            $export->column('website', function ($value, $original) {
                return strip_tags($value);
            });
            $export->column('spokepeople', function ($value, $original, $record) use ($_this) {
                $data = $_this->spokeperson_cache($record);
                if (!empty($data[0])) {
                    return $data[0]['name'] . ' ' . $data[0]['surname'];
                }

                return '';
            });
            $export->column('role', function ($value, $original, $record) use ($_this) {
                $data = $_this->spokeperson_cache($record);
                if (!empty($data[0])) {
                    return $data[0]['role'];
                }

                return '';
            });
            $export->column('email', function ($value, $original, $record) use ($_this) {
                $data = $_this->spokeperson_cache($record);
                if (!empty($data[0])) {
                    return $data[0]['email'];
                }

                return '';
            });
            $export->column('note', function ($value, $original, $record) use ($_this) {
                $data = $_this->spokeperson_cache($record);
                if (!empty($data[0])) {
                    return $data[0]['note'];
                }

                return '';
            });
            $export->column('step1', function ($value, $original, $record) use ($_this) {
                if (isset($record['museumID']) && !empty($record['museumID'])) {
                    return true;
                }

                return false;
            });
            $export->column('step2', function ($value, $original, $record) use ($_this) {
                if (is_bool($_this->wizard_cache($record['id'], 'spokeperson'))) {
                    return true;
                }

                return false;
            });
            $export->column('step3', function ($value, $original, $record) use ($_this) {
                if (is_bool($_this->wizard_cache($record['id'], 'application'))) {
                    return true;
                }

                return false;
            });
            $export->column('step3-1', function ($value, $original, $record) use ($_this) {
                if (!empty($_this->wizard_cache($record['id'], 'survey'))) {
                    return true;
                }

                return false;
            });
            $export->column('step4', function ($value, $original, $record) use ($_this) {
                if (is_bool($_this->wizard_cache($record['id'], 'editmuseumdata'))) {
                    return true;
                }

                return false;
            });
            $export->column('step5', function ($value, $original, $record) use ($_this) {
                if (is_bool($_this->wizard_cache($record['id'], 'museumdataforwikipedia'))) {
                    return true;
                }

                return false;
            });
            $export->column('step6', function ($value, $original, $record) use ($_this) {
                if (is_bool($_this->wizard_cache($record['id'], 'imageauthorization'))) {
                    return true;
                }

                return false;
            });
            $export->column('step7', function ($value, $original, $record) use ($_this) {
                if (is_bool($_this->wizard_cache($record['id'], 'imagesupload'))) {
                    return true;
                }

                return false;
            });
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(MuseumData::findOrFail($id));

        $show->column('museumID', __('messages.museum_data.museumWikidataID'));
        $show->column('name', __('messages.museum_data.name'));
        $show->column('address', __('messages.museum_data.address'));
        $show->column('email', __('messages.museum_data.email'));
        $show->column('website', __('messages.museum_data.website'));
        $show->column('municipality', __('messages.museum_data.municipality'));
        $show->column('types', __('messages.museum_data.types'));
        $show->column('owner', __('messages.museum_data.owner'));
        $show->column('description', __('messages.museum_data.description'));
        $show->column('authorizationLetter', __('messages.museum_data.authorizationLetter'));
        $show->column('manager', __('messages.museum_data.manager'));
        $show->column('letter', __('messages.museum_data.letter'));
        $show->column('description', __('messages.museum_data.description'));
        $show->column('created_at', __('messages.museum_data.created_at'));
        $show->column('updated_at', __('messages.museum_data.updated_at'));
        $show->column('history', __('messages.museum_data.history'));
        $show->column('institutionAssets', __('messages.museum_data.institutionAssets'));
        $show->column('bibliography', __('messages.museum_data.bibliography'));
        $show->column('notification', __('messages.museum_data.notification'));
        $show->column('survey', __('messages.museum_data.survey'));
        $show->images()->display(function ($images) {
            return json_decode($images, true);
        })->map(function ($path) {
            return request()->url() . '/storage/images/' . $path;
        })->image();

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new MuseumData());

        $form->text('museumID', __('messages.museum_data.museumWikidataID'));
        $form->text('name', __('messages.museum_data.name'));
        $form->text('address', __('messages.museum_data.address'));
        $form->email('email', __('messages.museum_data.email'));
        $form->url('website', __('messages.museum_data.website'));
        $form->text('municipality', __('messages.museum_data.municipality'));
        $form->types('types', __('messages.museum_data.types'));
        $form->text('owner', __('messages.museum_data.owner'));
        $form->text('manager', __('messages.museum_data.manager'));
        $form->textarea('description', __('messages.museum_data.description'));
        $form->textlink('authorizationLetter', __('messages.museum_data.authorizationLetter'));
        $form->textlink('letter', __('messages.museum_data.letter'));
        $form->jsonlink('images', __('messages.museum_data.images'));
        $form->textarea('history', __('messages.museum_data.history'));
        $form->textarea('institutionAssets', __('messages.museum_data.institutionAssets'));
        $form->textarea('bibliography', __('messages.museum_data.bibliography'));
        $form->spokepeopledata('spokepeople', __('messages.museum_data.spokepeople'));
        $form->radio('survey', __('messages.museum_data.survey'))->options([1 => __('Yes'), 0 => __('No')]);
        $form->radio('notification', __('messages.museum_data.notification'))->options([1 => __('Yes'), 0 => __('No')]);
        $form->text('created_at', __('messages.museum_data.created_at'));
        $form->text('updated_at', __('messages.museum_data.updated_at`'));

        $form->disableCreatingCheck();
        $form->disableViewCheck();

        $form->tools(function (Form\Tools $tools) {
            $tools->disableView();
        });

        return $form;
    }
}
