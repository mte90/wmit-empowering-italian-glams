<?php

namespace App\Admin\Actions;

use Encore\Admin\Actions\RowAction;
use Illuminate\Support\Facades\URL;

class View extends RowAction
{
    public $name = 'Show';

    public function href()
    {
        return URL::to('/') . '?step=resume&museum=' . $this->getKey();
    }
}
