<?php

namespace App\Http\Livewire;

use App\Http\Livewire\Steps\IntroStepComponent;
use App\Http\Livewire\Steps\MuseumStepComponent;
use App\Http\Livewire\Steps\SpokePersonStepComponent;
use App\Http\Livewire\Steps\ApplicationStepComponent;
use App\Http\Livewire\Steps\EditMuseumDataStepComponent;
use App\Http\Livewire\Steps\MuseumDataForWikipediaStepComponent;
use App\Http\Livewire\Steps\ImageAuthorizationStepComponent;
use App\Http\Livewire\Steps\ImagesUploadStepComponent;
use App\Http\Livewire\Steps\FinishStepComponent;
use Spatie\LivewireWizard\Components\WizardComponent;

class GlamsWizardComponent extends WizardComponent
{
    public function steps(): array
    {
        return [
            IntroStepComponent::class,
            MuseumStepComponent::class,
            SpokePersonStepComponent::class,
            ApplicationStepComponent::class,
            EditMuseumDataStepComponent::class,
            MuseumDataForWikipediaStepComponent::class,
            ImageAuthorizationStepComponent::class,
            ImagesUploadStepComponent::class,
            FinishStepComponent::class,
        ];
    }
}
