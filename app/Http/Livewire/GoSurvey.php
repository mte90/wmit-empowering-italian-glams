<?php

namespace App\Http\Livewire;

use LivewireUI\Modal\ModalComponent;
use Illuminate\Support\Facades\URL;
use App\Models\MuseumData;

class GoSurvey extends ModalComponent
{
    public $museum;

    public function mount($museum)
    {
        $this->museum = $museum;
    }

    public function goToSurvey()
    {
        return redirect()->to(MuseumData::generateURLSurvey($this->museum, '1'));
    }

    public function goNext()
    {
        return redirect()->to(URL::to('/') . '?step=editmuseumdata&museum=' . $this->museum);
    }

    public function render()
    {
        return view('livewire.go-survey')
            ->with([
                'museum' => $this->museum,
                'agreesurveyURL' => MuseumData::generateURLSurvey($this->museum, '1'),
            ]);
    }
}
