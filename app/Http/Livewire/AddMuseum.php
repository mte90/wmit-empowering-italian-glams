<?php

namespace App\Http\Livewire;

use LivewireUI\Modal\ModalComponent;
use App\Models\MuseumData;
use Illuminate\Support\Facades\URL;

class AddMuseum extends ModalComponent
{
    public $region;
    public $province;
    public $municipality;
    public $name;

    public $address = '';
    public $email = '';
    public $website = '';
    public $description = '';
    public $typology = array();

    protected $rules = [
        'name' => 'required|min:4',
        'address' => 'required|min:4',
        'email' => 'required|email',
    ];

    public function mount($region, $province, $municipality, $name)
    {
        $this->region = $region;
        $this->province = $province;
        $this->municipality = $municipality;
        $this->name = $name;
    }

    public function update()
    {
        if ($this->validate()) {
            $museumAlreadyExists = MuseumData::query()
            ->distinct()->select('*')
            ->where('name', $this->name)
            ->where('municipality', $this->municipality)
            ->get()->toArray();
            if (!empty($museumAlreadyExists[0])) {
                $this->addError('alreadyExists', __('messages.error_museum_already_exists', ['resumelink' => URL::to('/') . '?step=resume&museum=' . $museumAlreadyExists[0]['id']]));
                return;
            }

            $museumId = \App\Models\MuseumData::create([
                'name' => $this->name,
                'address' => $this->address,
                'email' => $this->email,
                'website' => $this->website,
                'municipality' => $this->municipality,
                'types' => json_encode(array_values($this->typology)),
                'description' => $this->description,
                'museumID' => '',
                'owner' => '',
                'manager' => '',
                'letter' => '',
                'authorizationLetter' => '',
                'images' => '',
                'bibliography' => '',
                'history' => '',
                'institutionAssets' => '',
                'notification' => true,
                'survey' => false
            ]);

            $this->closeModal();

            $this->emit('museumGetNewMuseum', $museumId->id);
        }
    }

    public function render()
    {
        $museum = new \App\Models\MuseumData();
        return view('livewire.add-museum')
            ->with([
                'region' => $this->region,
                'province' => $this->province,
                'municipality' => $this->municipality,
                'name' => $this->name,
                'typologies' => $museum->museumType
            ]);
    }
}
