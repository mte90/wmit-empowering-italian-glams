<?php

namespace App\Http\Livewire\Steps;

use Livewire\Livewire;
use Spatie\LivewireWizard\Components\StepComponent;
use Illuminate\Http\Request;
use App\Models\MuseumData;
use Illuminate\Support\Facades\URL;

class FinishStepComponent extends StepComponent
{

    public $museumID = '';

    public function stepInfo(): array
    {
        return [
            'label' => __('messages.finish_title'),
            'tab_status' => MuseumData::tabStatus(),
            'alias' => Livewire::getAlias(static::class)
        ];
    }

    public function boot(Request $request)
    {
        $this->museumID = MuseumData::getMuseum($request);
    }

    public function render()
    {
        $status = array(
            'spokeperson' => '',
            'application' => '',
            'editmuseumdata' => '',
            'museumdataforwikipedia' => '',
            'imageauthorization' => '',
            'imagesupload' => '',
            'error' => '',
            'notification' => '',
        );

        $museumdata = MuseumData::query()
            ->distinct()->select('*')
            ->where('museumID', '=', $this->museumID)
            ->orWhere('id', '=', $this->museumID)
            ->get()->toArray();

        if (isset($museumdata[0])) {
            $status = MuseumData::wizardFinished($museumdata[0]);
            if ($museumdata[0]['notification'] === 1) {
                $status['notification'] = '<span class="text-[#45c468] text-xl">✓</span> ';
                $status['notification'] .= __('messages.finish_list.yes_notifications', [
                    'notificationlink' => URL::to('/') . '?step=notification&museum=' . $this->museumID,
                ]);
            }

            $status['spokeperson'] = MuseumData::flagCheckGreen($status['spokeperson']);
            $status['application'] = MuseumData::flagCheckGreen($status['application']);
            $status['editmuseumdata'] = MuseumData::flagCheckGreen($status['editmuseumdata']);
            $status['museumdataforwikipedia'] = MuseumData::flagCheckGreen($status['museumdataforwikipedia']);
            $status['imageauthorization'] = MuseumData::flagCheckGreen($status['imageauthorization']);
            $status['imagesupload'] = MuseumData::flagCheckGreen($status['imagesupload']);
        } else {
            $status['error'] = __('messages.finish_notfound', ['museumid' => $this->museumID]);
        }

        return view('livewire.indexWizard.steps.finish')->with([
            'museum' => MuseumData::getMuseumLabel($this->museumID),
            'status' => $status,
        ]);
    }
}
