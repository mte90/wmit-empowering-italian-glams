<?php

namespace App\Http\Livewire\Steps;

use Livewire\Livewire;
use Spatie\LivewireWizard\Components\StepComponent;
use Livewire\WithFileUploads;
use Illuminate\Http\Request;
use App\Models\MuseumData;

class ImagesUploadStepComponent extends StepComponent
{

    public $images = [];
    public $museumID;

    use WithFileUploads;

    protected $rules = [
        'images.*' => 'required|mimes:jpg,jpeg,png,csv,xls,xlsx,tiff,tsv|max:30240',
        'images' => 'max:20',
    ];

    protected $messages = [];

    public function __construct()
    {
        $this->messages = [
            'images.required' => __('messages.imagesupload_error'),
            'images.mimes' => __('messages.imagesupload_mimes'),
        ];
    }

    public function stepInfo(): array
    {
        return [
            'label' => __('messages.imageasupload_title'),
            'tab_status' => MuseumData::tabStatus(),
            'alias' => Livewire::getAlias(static::class)
        ];
    }

    public function boot(Request $request)
    {
        $this->museumID = MuseumData::getMuseum($request);
    }

    public function render()
    {
        return view('livewire.indexWizard.steps.imagesupload')->with([
            'museum' => MuseumData::getMuseumLabel($this->museumID),
        ]);
    }

    public function submit()
    {
        if ($this->validate()) {
            $images = [];
            foreach ($this->images as $photo) {
                $ext = pathinfo($photo->getClientOriginalName());
                $path = preg_replace('/[^A-Za-z0-9-]/', '', $ext['filename']);
                $path .= '.' . $ext['extension'];
                $path = str_replace(' ', '', $path);
                $images[] = $photo->storeAs('images/' . $this->museumID, $path);
            }

            MuseumData::removePreviousUploadFiles(
                MuseumData::where('museumID', '=', $this->museumID)
                ->orWhere('id', '=', $this->museumID)
                ->select('images')
                ->get()->toArray()[0]['images']
            );

            MuseumData::where('museumID', '=', $this->museumID)
            ->orWhere('id', '=', $this->museumID)->update([
                'images' => json_encode($images),
            ]);

            $this->nextStep();
        }
    }

    public function redirectSurvey()
    {
        return redirect()->to(MuseumData::generateURLSurvey($this->museumID));
    }

    public function updated() {
        $this->resetErrorBag();
        $this->resetValidation();
    }
}
