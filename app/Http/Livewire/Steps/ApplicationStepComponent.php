<?php

namespace App\Http\Livewire\Steps;

use Livewire\Livewire;
use Spatie\LivewireWizard\Components\StepComponent;
use Livewire\WithFileUploads;
use Illuminate\Http\Request;
use App\Models\MuseumData;
use App\Models\Spokeperson;

class ApplicationStepComponent extends StepComponent
{

    public $letter = '';
    public $museumID;

    use WithFileUploads;

    public function stepInfo(): array
    {
        return [
            'label' => __('messages.application_title'),
            'tab_status' => MuseumData::tabStatus(),
            'alias' => Livewire::getAlias(static::class)
        ];
    }

    public function boot(Request $request)
    {
        $this->museumID = MuseumData::getMuseum($request);
    }

    public function render()
    {
        $email = Spokeperson::query()
        ->select('email')->where('museumID', $this->museumID)
        ->get()->toArray();

        if (!isset($email[0]['email'])) {
            redirect()->to('/?step=museum');
        }

        $email = $email[0]['email'];

        return view('livewire.indexWizard.steps.application')->with([
            'museum' => MuseumData::getMuseumLabel($this->museumID),
            'email' => $email,
        ]);
    }

    public function submit()
    {
        $toshow = true;
        if (!empty($this->letter) && $this->validate([
            'letter'     => 'mimetypes:application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/pdf,application/vnd.oasis.opendocument.text,application/zip,application/octet-stream|max:10240'
        ])) {
            $toshow = false;
            if (!is_string($this->letter)) {
                $ext = pathinfo($this->letter->getClientOriginalName());
                $path = preg_replace('/[^A-Za-z0-9-]/', '', MuseumData::getMuseumLabel($this->museumID));
                $path .= '.' . $ext['extension'];
                $path = str_replace(' ', '', $path);
                $path = $this->letter->storeAs('letters', $path);

                MuseumData::removePreviousUploadFiles(
                    json_encode(MuseumData::where('museumID', '=', $this->museumID)
                    ->orWhere('id', '=', $this->museumID)
                    ->select('letter')
                    ->get()->toArray()[0]['letter'])
                );

                MuseumData::where('museumID', '=', $this->museumID)
                ->orWhere('id', '=', $this->museumID)->update([
                    'letter' => $path,
                ]);

                $toshow = true;
            }
        }

        if ($toshow) {
            $this->goSurveyModal();
        }
    }

    public function redirectSurvey()
    {
        return redirect()->to(MuseumData::generateURLSurvey($this->museumID));
    }

    public function goSurveyModal()
    {
        $this->emit('openModal', 'go-survey', [
            'museum' => $this->museumID,
        ]);
    }

    public function updated() {
        $this->resetErrorBag();
        $this->resetValidation();
    }
}
