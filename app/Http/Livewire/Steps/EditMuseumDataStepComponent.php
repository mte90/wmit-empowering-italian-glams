<?php

namespace App\Http\Livewire\Steps;

use Livewire\Livewire;
use Spatie\LivewireWizard\Components\StepComponent;
use App\Models\MuseumData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class EditMuseumDataStepComponent extends StepComponent
{
    public $description = '';
    public $address = '';
    public $email = '';
    public $website = '';
    public $manager = '';
    public $owner = '';
    public $typology = [];
    public $museumID = '';

    protected $rules = [
        'address' => 'required',
        'description' => 'required',
        'website' => 'url'
    ];

    public function stepInfo(): array
    {
        return [
            'label' => __('messages.editmuseumdata_title'),
            'tab_status' => MuseumData::tabStatus(),
            'alias' => Livewire::getAlias(static::class)
        ];
    }

    public function boot(Request $request)
    {
        $this->museumID = MuseumData::getMuseum($request);
        if ($this->museumID[0] === 'Q') {
            $response = Http::get('https://www.wikidata.org/wiki/Special:EntityData/' . $this->museumID . '.json');
            $json = $response->json();
            if (!is_null($json)) {
                if (isset($json['entities'][$this->museumID])) {
                        $json = $json['entities'][$this->museumID];
                } else {
                        $json = $json['entities'][array_key_first($json['entities'])];
                }
                $locale = config('app.locale');
                if (isset($json['descriptions'][$locale]['value'])) {
                    $this->description = $json['descriptions'][$locale]['value'];
                }
                if (isset($json['claims']['P6375'][0]['mainsnak']['datavalue']['value'])) {
                    $this->address = $json['claims']['P6375'][0]['mainsnak']['datavalue']['value']['text'];
                }
                if (isset($json['claims']['P968'][0]['mainsnak']['datavalue']['value'])) {
                    $this->email = $json['claims']['P968'][0]['mainsnak']['datavalue']['value'];
                    $this->email = str_replace('mailto:', '', $this->email);
                }
                if (isset($json['claims']['P856'][0]['mainsnak']['datavalue']['value'])) {
                    $this->website = $json['claims']['P856'][0]['mainsnak']['datavalue']['value'];
                }
                if (isset($json['claims']['P137'][0]['mainsnak']['datavalue']['value'])) {
                    $this->manager = $json['claims']['P137'][0]['mainsnak']['datavalue']['value'];
                }
                if (isset($json['claims']['P127'][0]['mainsnak']['datavalue']['value'])) {
                    $this->owner = $json['claims']['P127'][0]['mainsnak']['datavalue']['value'];
                }
            }
        } else {
            redirect()->to('/?step=museumdataforwikipedia');
        }
    }

    public function render()
    {
        $museum = new \App\Models\MuseumData();

        return view('livewire.indexWizard.steps.editmuseumdata')->with(
            [
                'typologies' => $museum->museumType,
                'museum' => MuseumData::getMuseumLabel($this->museumID),
            ]
        );
    }

    public function submit()
    {
        if ($this->validate()) {
            MuseumData::where('museumID', '=', $this->museumID)
            ->orWhere('id', '=', $this->museumID)->update([
                'description' => $this->description,
                'types' => $this->typology,
                'website' => $this->website,
                'address' => $this->address,
                'owner' => $this->owner,
                'manager' => $this->manager,
            ]);

            $this->nextStep();
        }
    }

    public function redirectSurvey()
    {
        $filled = MuseumData::redirectSurvey($this->museumID);

        if (is_bool($filled) && $filled) {
            $this->nextStep();
        }
    }
}
