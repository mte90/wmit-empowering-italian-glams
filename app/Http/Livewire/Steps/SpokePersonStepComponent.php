<?php

namespace App\Http\Livewire\Steps;

use Notification;
use Livewire\Livewire;
use App\Notifications\ApplicationNotification;
use Spatie\LivewireWizard\Components\StepComponent;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\HtmlString;
use Illuminate\Http\Request;
use App\Models\MuseumData;
use App\Models\Spokeperson;

class SpokePersonStepComponent extends StepComponent
{

    public $name = '';
    public $surname = '';
    public $role = '';
    public $email = '';
    public $note = '';
    public $museumID = '';

    protected $rules = [
        'name' => 'required|min:4',
        'surname' => 'required|min:4',
        'role' => 'required|min:4',
        'email' => 'required|email',
    ];

    public function stepInfo(): array
    {
        return [
            'label' => __('messages.spokeperson_title'),
            'tab_status' => MuseumData::tabStatus(),
            'alias' => Livewire::getAlias(static::class)
        ];
    }

    public function boot(Request $request)
    {
        $this->museumID = MuseumData::getMuseum($request);
        $exists = Spokeperson::where('museumID', '=', $this->museumID)
            ->select('*')
            ->get()->toArray();
        if (!empty($exists[0])) {
            $this->name = $exists[0]['name'];
            $this->surname = $exists[0]['surname'];
            $this->role = $exists[0]['role'];
            $this->email = $exists[0]['email'];
            $this->note = $exists[0]['note'];
        }
    }

    public function render()
    {
        return view('livewire.indexWizard.steps.spokeperson')->with([
            'museum' => MuseumData::getMuseumLabel($this->museumID),
        ]);
    }

    public function submit()
    {
        if ($this->validate()) {
            $exists = Spokeperson::where('museumID', '=', $this->museumID)
            ->select('id')
            ->get()->toArray();
            if (empty($exists)) {
                Spokeperson::create([
                    'museumID' => $this->museumID,
                    'name' => $this->name,
                    'surname' => $this->surname,
                    'role' => $this->role,
                    'email' => $this->email,
                    'note' => $this->note,
                ]);

                $email = [
                    'subject' => __('messages.application_notification.subject'),
                    'body' => new HtmlString(__('messages.application_notification.body', [
                        'museumname' => MuseumData::getMuseumLabel($this->museumID),
                        'spokepeople' => $this->name . ' ' . $this->surname,
                        'resumelink' => URL::to('/') . '?step=resume&museum=' . $this->museumID
                    ]))
                ];

                Notification::route('mail', $this->email)->notify(new ApplicationNotification($email));
            } else {
                Spokeperson::where('museumID', '=', $this->museumID)
                    ->update([
                    'museumID' => $this->museumID,
                    'name' => $this->name,
                    'surname' => $this->surname,
                    'role' => $this->role,
                    'email' => $this->email,
                    'note' => $this->note,
                ]);
            }
            $exists = MuseumData::where('museumID', '=', $this->museumID)
            ->orWhere('id', '=', $this->museumID)
            ->select('id')
            ->get()->toArray();
            if (empty($exists)) {
                MuseumData::create([
                    'name' => MuseumData::getMuseumLabel($this->museumID),
                    'address' => '',
                    'email' => '',
                    'website' => '',
                    'municipality' => '',
                    'types' => '',
                    'description' => '',
                    'museumID' => $this->museumID,
                    'owner' => '',
                    'manager' => '',
                    'letter' => '',
                    'authorizationLetter' => '',
                    'images' => '',
                    'bibliography' => '',
                    'history' => '',
                    'institutionAssets' => '',
                    'notification' => true,
                    'survey' => false
                ]);
            }

            $this->nextStep();
        }
    }
}
