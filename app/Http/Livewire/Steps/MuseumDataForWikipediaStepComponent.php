<?php

namespace App\Http\Livewire\Steps;

use Livewire\Livewire;
use Spatie\LivewireWizard\Components\StepComponent;
use App\Models\MuseumData;
use Illuminate\Http\Request;

class MuseumDataForWikipediaStepComponent extends StepComponent
{
    public $history = '';
    public $institutionAssets = '';
    public $bibliography = '';
    public $museumID;

    public function stepInfo(): array
    {
        return [
            'label' => __('messages.museumdataforwikipedia_title'),
            'tab_status' => MuseumData::tabStatus(),
            'alias' => Livewire::getAlias(static::class)
        ];
    }

    public function boot(Request $request)
    {
        $this->museumID = MuseumData::getMuseum($request);
        $museumdata = MuseumData::query()
            ->distinct()->select('*')
            ->where('museumID', $this->museumID)
            ->orWhere('id', $this->museumID)
            ->get()->toArray();

        if (isset($museumdata[0])) {
            $this->history = $museumdata[0]['history'];
            $this->institutionAssets = $museumdata[0]['institutionAssets'];
            $this->bibliography = $museumdata[0]['bibliography'];
        }
    }

    public function render()
    {
        return view('livewire.indexWizard.steps.museumdataforwikipedia')->with([
            'museum' => MuseumData::getMuseumLabel($this->museumID),
        ]);
    }

    public function submit()
    {
        MuseumData::where('museumID', '=', $this->museumID)
        ->orWhere('id', '=', $this->museumID)->update([
            'history' => $this->history,
            'institutionAssets' => $this->institutionAssets,
            'bibliography' => $this->bibliography,
        ]);

        $this->nextStep();
    }

    public function redirectSurvey()
    {
        $filled = MuseumData::redirectSurvey($this->museumID);

        if (is_bool($filled) && $filled) {
            $this->nextStep();
        }
    }
}
