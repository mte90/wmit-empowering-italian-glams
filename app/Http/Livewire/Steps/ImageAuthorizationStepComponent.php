<?php

namespace App\Http\Livewire\Steps;

use Livewire\Livewire;
use Spatie\LivewireWizard\Components\StepComponent;
use Livewire\WithFileUploads;
use Illuminate\Http\Request;
use App\Models\MuseumData;

class ImageAuthorizationStepComponent extends StepComponent
{

    public $authorizationLetter = '';
    public $museumID;

    use WithFileUploads;

    public function stepInfo(): array
    {
        return [
            'label' => __('messages.imageauthorization_title'),
            'tab_status' => MuseumData::tabStatus(),
            'alias' => Livewire::getAlias(static::class)
        ];
    }

    public function boot(Request $request)
    {
        $this->museumID = MuseumData::getMuseum($request);
    }

    public function render()
    {
        return view('livewire.indexWizard.steps.imageauthorization')->with([
            'museum' => MuseumData::getMuseumLabel($this->museumID),
        ]);
    }

    public function submit()
    {
        if ($this->validate([
            'authorizationLetter' => 'mimetypes:application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/pdf,application/vnd.oasis.opendocument.text,application/zip,application/octet-stream|max:10240',
        ])) {
            if (!is_string($this->authorizationLetter)) {
                $ext = pathinfo($this->authorizationLetter->getClientOriginalName());
                $path = preg_replace('/[^A-Za-z0-9-]/', '', MuseumData::getMuseumLabel($this->museumID));
                $path .= '-autorizzazione.' . $ext['extension'];
                $path = str_replace(' ', '', $path);
                $path = $this->authorizationLetter->storeAs('authorizationLetter', $path);

                MuseumData::removePreviousUploadFiles(
                    json_encode(MuseumData::where('museumID', '=', $this->museumID)
                    ->orWhere('id', '=', $this->museumID)
                    ->select('authorizationLetter')
                    ->get()->toArray()[0]['authorizationLetter'])
                );

                MuseumData::where('museumID', '=', $this->museumID)
                ->orWhere('id', '=', $this->museumID)->update([
                    'authorizationLetter' => $path,
                ]);

                $this->nextStep();
            }
        }
    }

    public function updated() {
        $this->resetErrorBag();
        $this->resetValidation();
    }
}
