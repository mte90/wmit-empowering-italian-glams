<?php

namespace App\Http\Livewire\Steps;

use Livewire\Livewire;
use Spatie\LivewireWizard\Components\StepComponent;
use App\Models\Italia;
use App\Models\MuseumData;
use App\Models\MuseiComuni;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class MuseumStepComponent extends StepComponent
{
    public $regionPicked = '';
    public $provincePicked = '';
    public $municipalityPicked = '';
    public $museumPicked = '';
    public $newMuseumPicked = '';

    protected $listeners = ['museumGetNewMuseum' => 'newMuseumManual'];

    public function stepInfo(): array
    {
        return [
            'label' => __('messages.museum_title_step'),
            'tab_status' => MuseumData::wizardFinished(''),
            'alias' => Livewire::getAlias(static::class)
        ];
    }

    public function render(Request $request)
    {
        // TODO: migrate to something by country
        $regionsList = array(
            __('lazio') => __('Lazio'),
            __('abruzzo') => __('Abruzzo'),
            __('umbria') => __('Umbria'),
            __('campania') => __('Campania'),
            __('molise') => __('Molise'),
            __('puglia') => __('Puglia'),
            __('calabria') => __('Calabria'),
            __('sicilia') => __('Sicilia'),
            __('basilicata') => __('Basilicata'),
            __('sardegna') => __('Sardegna'),
            __('toscana') => __('Toscana'),
            __('marche') => __('Marche'),
            __('emilia-romagna') => __('Emilia Romagna'),
            __('valle d\'aosta') => __('Valle d\'Aosta'),
            __('piemonte') => __('Piemonte'),
            __('liguria') => __('Liguria'),
            __('lombardia') => __('Lombardia'),
            __('friuli-venezia giulia') => __('Friuli Venezia Giulia'),
            __('veneto') => __('Veneto'),
            __('trentino-alto adige/südtirol') => __('Trentino Alto Adige/Südtirol'),
        );
        ksort($regionsList);

        $provincesList = array();

        if (!is_null($request->get('region'))) {
            $this->regionPicked = $request->get('region');
        }

        if (!is_null($request->get('province'))) {
            $this->provincePicked = $request->get('province');
        }

        if (!is_null($request->get('municipality'))) {
            $this->municipalityPicked = $request->get('municipality');
        }

        if (!empty($this->regionPicked)) {
            $provincesList = Italia::query()
                ->distinct()->select('provincia')
                ->where('regione', $this->regionPicked)
                ->orderBy('provincia')->get()->toArray();
        }

        $municipalitiesList = array();

        if (!empty($this->provincePicked)) {
            $municipalitiesList = Italia::query()
                ->distinct()->select('comune')
                ->where('provincia', $this->provincePicked)
                ->orderBy('comune')->get()->toArray();
        }

        $museumsList = array();

        if (!empty($this->municipalityPicked)) {
            $museumsList = MuseiComuni::query()
                ->distinct()->select('*')
                ->where('comune', $this->municipalityPicked)
                ->orderBy('nome')->get()->toArray();
        }

        if (filter_var($this->museumPicked, FILTER_VALIDATE_INT) || filter_var($this->newMuseumPicked, FILTER_VALIDATE_INT)) {
            $this->museumPicked = '';
            $this->newMuseumPicked = '';
        }

        return view('livewire.indexWizard.steps.museum')->with([
            'regionsList' => $regionsList,
            'provincesList' => $provincesList,
            'municipalitiesList' => $municipalitiesList,
            'museumsList' => $museumsList
        ]);
    }

    public function newMuseumManual($newMuseum)
    {
        $this->newMuseumPicked = $newMuseum;
        Cookie::queue('museum', $this->newMuseumPicked);
        $this->nextStep();
    }

    public function deselectMuseum()
    {
        $this->museumPicked = [];
    }

    public function submit()
    {
        // TODO: if already compiled block the process
        Cookie::queue('museum', $this->museumPicked);

        $this->nextStep();
    }

    public function addMuseumModal()
    {
        $this->emit('openModal', 'add-museum', [
            'region' => $this->regionPicked,
            'province' => $this->provincePicked,
            'municipality' => $this->municipalityPicked,
            'name' => $this->newMuseumPicked
        ]);
    }
}
