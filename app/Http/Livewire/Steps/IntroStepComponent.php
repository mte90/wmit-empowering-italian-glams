<?php

namespace App\Http\Livewire\Steps;

use Livewire\Livewire;
use App\Models\MuseumData;
use Spatie\LivewireWizard\Components\StepComponent;

class IntroStepComponent extends StepComponent
{

    public function stepInfo(): array
    {
        return [
            'label' => __('messages.tab_title'),
            'tab_status' => MuseumData::wizardFinished(''),
            'alias' => Livewire::getAlias(static::class)
        ];
    }

    public function render()
    {
        return view('livewire.indexWizard.steps.intro');
    }
}
