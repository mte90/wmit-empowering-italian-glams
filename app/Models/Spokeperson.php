<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Spokeperson extends Model
{
    use HasFactory;

    protected $fillable = [
        'museumID',
        'name',
        'surname',
        'email',
        'role',
        'note',
    ];

    protected $guarded = [
        'id',
    ];
}
