<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MuseiComuni extends Model
{
    protected $table = 'MuseiComuni';
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'wikidata',
        'nome',
        'comuneQ',
        'comune',
        'provincia',
        'regione',
    ];
}
