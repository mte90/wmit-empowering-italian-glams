<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Spokeperson;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\File;

class MuseumData extends Model
{
    use HasFactory;

    protected $fillable = [
        'museumID',
        'name',
        'address',
        'email',
        'municipality',
        'types',
        'owner',
        'manager',
        'letter',
        'images',
        'authorizationLetter',
        'history',
        'institutionAssets',
        'bibliography',
        'notification',
        'survey',
    ];

    public $museumType = [];

    protected $guarded = [
        'id',
    ];

    public function __construct()
    {
        $this->museumType = [
            'OTHER' => __('messages.museum_type_other'),
            'ANCIENT_ART' => __('messages.museum_type_art_ancient'),
            'CONTEMPORARY_ART' => __('messages.museum_type_contemporary_art'),
            'RELIGIOUS' => __('messages.museum_type_religious'),
            'ARCHEOLOGICAL' => __('messages.museum_type_archeological'),
            'HISTORICAL' => __('messages.museum_type_historical'),
            'NATURAL_HISTORY' => __('messages.museum_type_natural_history'),
            'TECHNOLOGY' => __('messages.museum_type_technology'),
            'ETHNOGRAPHIC' => __('messages.museum_type_ethnographic'),
            'ANTHROPOLOGICAL' => __('messages.museum_type_anthropological'),
            'SPECIALIZED' => __('messages.museum_type_specialized'),
            'INDUSTRY' => __('messages.museum_type_industry'),
            'HOUSE' => __('messages.museum_type_house'),
            'ARCHEOLOGIC_AREA' => __('messages.museum_type_archeologic_area'),
            'ARCHEOLOGIC_PARK' => __('messages.museum_type_archeologic_park'),
            'PALACE' => __('messages.museum_type_palace'),
            'RELIGIOUS_PALACE' => __('messages.museum_type_religious_palace'),
            'INDUSTRIAL_ARCHEOLOGY' => __('messages.museum_type_industrial_archeology'),
            'SCIENTIFIC' => __('messages.museum_type_scientific'),
            'MODERN_ART' => __('messages.museum_type_modern_art'),
            'ARCHEOLOGIC_PARK' => __('messages.museum_type_archeologic_park'),
            'ECOMUSEUM' => __('messages.museum_type_ecomuseum'),
            'MARITIME' => __('messages.museum_type_maritime'),
            'DIOCESAN' => __('messages.museum_type_diocesan'),
            'PLASTER' => __('messages.museum_type_plaster'),
            'PICTURE' => __('messages.museum_type_picture'),
            'ENTERPRISE' => __('messages.museum_type_enterprise'),
            'LITERARY' => __('messages.museum_type_literary'),
            'UNIVERSITARY' => __('messages.museum_type_universitary'),
            'MILITARY' => __('messages.museum_type_military'),
        ];
    }

    /**
    * The "delete" method of the model.
    *
    * @return void
    */
    public function delete()
    {
        DB::table('spokepeople')->where('museumID', '=', $this->museumID)->orWhere('museumID', '=', $this->id)->delete();
        parent::delete();
    }

    public static function getMuseumLabel($museumID, $component = '')
    {
        $museumLabel = 'Error';
        if (!empty($museumID) && $museumID[0] === 'Q') {
            $museumQuery = MuseiComuni::query()
            ->distinct()->select('nome')
            ->where('wikidata', $museumID)
            ->get()->toArray();

            if (!empty($museumQuery[0])) {
                $museumLabel = $museumQuery[0]['nome'];
            }
        } else {
            $museumQuery = MuseumData::query()
            ->distinct()->select('name')
            ->where('id', $museumID)
            ->get()->toArray();

            if (!empty($museumQuery[0])) {
                $museumLabel = $museumQuery[0]['name'];
            }
        }

        if ($museumLabel === 'Error') {
            if (!empty($component)) {
                $component->emitUp('nextStep', 'museum');
            } else {
                redirect()->to('/?step=museum');
            }
        }

        return $museumLabel;
    }

    public static function getMuseum($request)
    {
        $museumID = '';
        if (is_string($request->get('museum'))) {
            $museumID = $request->get('museum');
            Cookie::queue('museum', $museumID);
        } else {
            $museumID = Cookie::get('museum');
        }

        return $museumID;
    }

    public static function museumExists($request)
    {
        $museumID = self::getMuseum($request);

        if (empty($museumID) || !isset($museumID[0])) {
            return false;
        }

        if ($museumID[0] === 'Q') {
            $exists = MuseiComuni::query()
            ->distinct()->select('nome')
            ->where('wikidata', $museumID)
            ->get()->toArray();
        } else {
            $exists = MuseumData::query()
            ->distinct()->select('name')
            ->where('id', $museumID)
            ->get()->toArray();
        }

        if (empty($exists)) {
            return false;
        }

        return true;
    }

    public static function wizardFinished($data)
    {
        $status = array(
            'spokeperson' => '<span class="text-[#ff0000] text-xl">X</span> ',
            'application' => '<span class="text-[#ff0000] text-xl">X</span> ',
            'editmuseumdata' => '<span class="text-[#ff0000] text-xl">X</span> ',
            'museumdataforwikipedia' => '<span class="text-[#ff0000] text-xl">X</span> ',
            'imageauthorization' => '<span class="text-[#ff0000] text-xl">X</span> ',
            'imagesupload' => '<span class="text-[#ff0000] text-xl">X</span> ',
            'error' => '',
            'intro' => '',
            'museum' => '',
            'finish' => '',
            'notification' => '<span class="text-[#ff0000] text-xl">X</span> ' . __('messages.finish_list.no_notifications'),
            'total' => 0
        );

        if (empty($data)) {
            $data = MuseumData::getMuseum(app()->request);
        }

        if (is_string($data)) {
            $data = MuseumData::query()
            ->distinct()->select('*')
            ->where('museumID', '=', $data)
            ->orWhere('id', '=', $data)
            ->get()->toArray();
        }

        if (empty($data)) {
            return $status;
        }

        if (isset($data[0])) {
            $data = $data[0];
        }

        $dataSpokepeople = Spokeperson::query()
        ->distinct()->select('*')
        ->where('museumID', $data['museumID'])
        ->orWhere('museumID', '=', $data['id'])
        ->get()->toArray();

        if (!empty($dataSpokepeople)) {
            $status['spokeperson'] = true;
            $status['total']++;
        }

        if (isset($data) && ! empty($data['letter'])) {
            $status['application'] = true;
            $status['total']++;
        }

        if (isset($data) && (! empty($data['description']) || empty($data['museumID'])) ) {
            $status['editmuseumdata'] = true;
            $status['total']++;
        }

        if (isset($data) && ! empty($data['history'])) {
            $status['museumdataforwikipedia'] = true;
            $status['total']++;
        }

        if (isset($data) && ! empty($data['authorizationLetter'])) {
            $status['imageauthorization'] = true;
            $status['total']++;
        }

        if (isset($data) && ! empty($data['images']) && $data['images']!=='[]') {
            $status['imagesupload'] = true;
            $status['total']++;
        }

        if ($status['total'] === 6 && $data['notification'] === 1 ) {
            MuseumData::where('museumID', '=', $data['museumID'])
            ->orWhere('id', '=', $data['id'])->update([
                'notification' => false,
            ]);
        }

        return $status;
    }

    public static function flagCheckGreen($flag)
    {
        if (is_bool($flag) && $flag) {
            return '<span class="text-[#45c468] text-xl">✓</span>';
        }

        return $flag;
    }

    public static function tabStatus()
    {
        $status = self::wizardFinished('');
        foreach ($status as $key => $item) {
            $status[$key] = self::flagCheckGreen($item);
        }

        return $status;
    }

    public static function generateURLSurvey($museumID, $join = '2')
    {
        $url = config('app.redirect_survey');
        $dataSpokepeople = Spokeperson::query()
        ->distinct()->select('*')
        ->where('museumID', $museumID)
        ->get()->toArray();
        $dataSpokepeople = $dataSpokepeople[0];

        $url = str_replace('{nomeistituzione}', self::getMuseumLabel($museumID), $url);
        $url = str_replace('{nomereferente}', $dataSpokepeople['name'] . ' '. $dataSpokepeople['surname'], $url);
        $url = str_replace('{ruoloreferente}', $dataSpokepeople['role'], $url);
        $url = str_replace('{emailreferente}', $dataSpokepeople['email'], $url);
        $url = str_replace('{museumid}', $museumID, $url);
        $url = str_replace('{adesione}', $join, $url); // 1 for yes, 2 for no
        return $url;
    }

    public static function removePreviousUploadFiles($jsonList)
    {
        $jsonList = json_decode($jsonList);
        if (is_array($jsonList) && !empty($jsonList)) {
            foreach ($jsonList as $item) {
                $item = storage_path('app/' . $item);
                if (File::exists($item)) {
                    unlink($item);
                }
            }
        }
    }

    public static function redirectSurvey($museumID)
    {
        $survey = MuseumData::query()->distinct()->select('survey')
        ->where('museumID', '=', $museumID)
        ->orWhere('id', '=', $museumID)
        ->get()->toArray();
        $survey = $survey[0]['survey'];
        if (!empty($survey) && $survey === 1) {
            return true;
        }

        return redirect()->to(MuseumData::generateURLSurvey($museumID));
    }
}
