<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Italia extends Model
{
    protected $table = 'italia';
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'wikidata',
        'regione',
        'comune',
        'provincia',
    ];
}
