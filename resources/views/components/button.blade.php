<button class="inline-flex items-center justify-center whitespace-nowrap border border-transparent bg-[#3366cc] px-4 py-2 text-sm font-bold text-white shadow-sm hover:bg-[#447ff5]"
    {{ $attributes->merge(['type' => 'submit', 'class' => 'button'])->except(['label']) }}
>
    {{ $label ?? 'Submit' }}
</button>
