<div class="form-group {!! !$errors->has($label) ?: 'has-error' !!}">
<label for="{{$id}}" class="col-sm-2 control-label">{{$label}}</label>

<div class="col-sm-6">
@include('admin::form.error')

<ul>
<li><b>{!! __('messages.spokeperson_name')!!}</b>: {!!$data['name']!!}</li>
<li><b>{!! __('messages.spokeperson_surname')!!}</b>: {!!$data['surname']!!}</li>
<li><b>{!! __('messages.spokeperson_role')!!}</b>: {!!$data['role']!!}</li>
<li><b>{!! __('messages.spokeperson_email')!!}</b>: {!!$data['email']!!}</li>
<li><b>{!! __('messages.spokeperson_note')!!}</b>: {!!$data['note']!!}</li>
</ul>
</div>
</div>
