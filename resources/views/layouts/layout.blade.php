<!doctype html>
<html lang="">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ __('messages.title') }}</title>
    @vite(['resources/js/app.js'])
    @vite(['resources/css/app.css'])
    <livewire:styles />
</head>
<body class="bg-gray-100">
<div class="max-w-7xl mx-auto sm:px-6 lg:px-8 pt-8">
    <div class="flex items-center justify-between py-6 md:justify-start md:space-x-10">
        <div class="flex justify-start lg:w-0 lg:flex-1">
            <h1 class="text-3xl font-extrabold tracking-tight text-slate-900">{{ __('messages.title') }}</h1>
        </div>
        <div class="justify-end md:flex md:flex-1 lg:w-0">
          <a href="{!! config('app.faq') !!}" target="_blank" class="ml-8 inline-flex items-center justify-center whitespace-nowrap border border-transparent bg-[#3366cc] px-4 py-2 text-sm font-bold text-white shadow-sm hover:bg-[#447ff5]">{{ __('messages.faq') }}</a>
        </div>
    </div>
    <div class="bg-white p-[15px] border border-[#a7d7f9] rounded-lg">
        {{ $slot }}
    </div>
    <div class="text-xs">{!! __('messages.footer', ['anchor' => '<a class="text-[#0746ae]" href="https://wiki.wikimedia.it/wiki/Empowering_Italian_GLAMs">', 'closeanchor'=>'</a>']) !!} | {!! __('messages.privacy_policy', ['anchor' => '<a class="text-[#0746ae]" href="https://www.wikimedia.it/privacy/">', 'closeanchor'=>'</a>']) !!}</div>
</div>
@livewire('livewire-ui-modal')
<livewire:scripts />
</body>
</html>
