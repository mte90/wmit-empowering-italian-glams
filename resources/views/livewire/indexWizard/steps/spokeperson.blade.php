<div>
    @include('livewire.indexWizard.navigation')

    <div class="border-t border-[#a7d7f9] pt-2 spokeperson">
        <div class="space-y-4">
            <h2 class="text-lg">{{ __('messages.spokeperson_subtitle') }}</h2>
            <h3 class="text-lg text-center">{{ $museum }}</h3>
            <p class="!my-0 font-bold">{{ __('messages.spokeperson_title_list') }}</p>

            <div>
            @if ($errors->any())
                <span class="block error">
                {{__('messages.error_fields')}}
                </span>
            @endif
            </div>

            <label for="name">{{__('messages.spokeperson_name')}}<span class="text-red-600 text-xs relative -top-2">*</span>:</label><input type="text" name="name" id="name" wire:model="name" size="40"><br>
            <label for="surname">{{__('messages.spokeperson_surname')}}<span class="text-red-600 text-xs relative -top-2">*</span>:</label><input type="text" name="surname" id="surname" wire:model="surname" size="40"><br>
            <label for="role">{{__('messages.spokeperson_role')}}<span class="text-red-600 text-xs relative -top-2">*</span>:</label><input type="text" name="role" id="role" wire:model="role" size="40"><br>
            <label for="email">{{__('messages.spokeperson_email')}}<span class="text-red-600 text-xs relative -top-2">*</span>:</label><input type="email" name="email" id="email" wire:model="email" size="30"><br>
            <label for="note">{{__('messages.spokeperson_note')}}</label><br>
            <textarea name="note" id="note" wire:model="note" rows="5" cols="100%"></textarea><br>

            <p class="text-xs">
                {!! __('messages.spokeperson_privacy') !!}
            </p>

            <x-button wire:click="submit" label="{{ __('messages.continue') }}"></x-button>
        </div>
    </div>

</div>
