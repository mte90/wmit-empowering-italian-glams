<div>
    @include('livewire.indexWizard.navigation')

    <div class="border-t border-[#a7d7f9] pt-2 spokeperson">
        <div class="space-y-4">
            <h2 class="text-lg">{{ __('messages.imageauthorization_subtitle') }}</h2>
            <h3 class="text-lg text-center">{{ $museum }}</h3>
            <ul class="list-disc list-inside">
                <li>{!! __('messages.imageauthorization_list.1') !!}</li>
                <li><label for="authorizationLetter">{{ __('messages.imageauthorization_list.2') }}:</label><input type="file" name="authorizationLetter" id="authorizationLetter" wire:model="authorizationLetter"><br>
                @error('authorizationLetter') <span class="block error">{{ $message }}</span> @enderror</li>
            </ul>

            <p>{!! __('messages.imageauthorization_list.other') !!}</p>

            <x-button wire:click="submit" label="{{ __('messages.continue') }}"></x-button>
            <x-button wire:click="nextStep" label="{{ __('messages.jump') }}"></x-button>
        </div>
    </div>

</div>
