<div>
    @include('livewire.indexWizard.navigation')

    <div class="border-t border-[#a7d7f9] pt-2">
        <div class="space-y-4">
            <h2 class="text-xl">{{ __('messages.museum_subtitle') }}</h2>
            <h3 class="text-lg">{{ __('messages.choose_region_title') }}</h2>
            <select wire:model="regionPicked" class="form-control" name="regions" required autocomplete="off">
                <option>{{ __('messages.pick_a_region') }}</option>
                    @foreach($regionsList as $key => $region)
                        <option value="{{$key}}">{{$region}}</option>
                    @endforeach
            </select>
            <h2 class="text-lg">{{ __('messages.choose_province_title') }}</h2>
            <select wire:model="provincePicked" class="form-control" name="province" required autocomplete="off"
                @if (empty($regionPicked))
                    disabled
                @endif
            >
                <option>{{ __('messages.pick_a_province') }}</option>
                @if (!empty($regionPicked))
                    @foreach($provincesList as $province)
                        <option value="{{$province['provincia']}}">{{$province['provincia']}}</option>
                    @endforeach
                @endif
            </select>
            <h2 class="text-lg">{{ __('messages.choose_municipality_title') }}</h2>
            <select wire:model="municipalityPicked" class="form-control" name="municipality" required autocomplete="off"
                @if (empty($provincePicked))
                    disabled
                @endif
            >
                <option>{{ __('messages.pick_a_municipality') }}</option>
                @if (!empty($provincePicked))
                    @foreach($municipalitiesList as $municipality)
                        <option value="{{$municipality['comune']}}">{{ucwords(strtolower($municipality['comune']))}}</option>
                    @endforeach
                @endif
            </select>
                @if (!empty($municipalityPicked))
                    <div class="museumslist">
                        @if (!empty($museumsList))
                            @foreach($museumsList as $museum)
                                <input type="radio" wire:model="museumPicked" name="museum[]" value="{{$museum['wikidata']}}" id="{{$museum['wikidata']}}" autocomplete="off"/>
                                <label for="{{$museum['wikidata']}}">{{$museum['nome']}}</label><br>
                            @endforeach
                        @endif
                    </div>

                    @php
                        $showContinue = false;
                        if (!empty($museumPicked)) {
                            $showContinue = true;
                        }
                    @endphp

                    @if (!empty($museumPicked))
                        <x-button class="border border-transparent bg-[#3366cc] px-4 py-2 text-base font-medium text-white shadow-sm hover:bg-[#447ff5]" label="{{__('messages.deselect')}}" wire:click="deselectMuseum" />
                        <h3 class="text-lg">{{ __('messages.museum_missing') }}!</h3>
                    @else
                        <h3 class="text-lg">{{ __('messages.if_museum_not_exist_title') }}</h3>
                        <input type="text" name="newMuseum" wire:model="newMuseumPicked" size="60">

                        @if (!empty($museumPicked) || !empty($newMuseumPicked))
                            <x-button wire:click="addMuseumModal" label="{{ __('messages.add_museum') }}"></x-button>
                        @endif
                    @endif

                    @if ($showContinue)
                        <x-button class="border border-transparent bg-[#3366cc] px-4 py-2 text-base font-medium text-white shadow-sm hover:bg-[#447ff5]" label="{{__('messages.continue')}}" wire:click="submit" />
                    @endif
                @endif

        </div>
    </div>

</div>
