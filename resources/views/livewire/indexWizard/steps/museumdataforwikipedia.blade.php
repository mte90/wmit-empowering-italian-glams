<div>
    @include('livewire.indexWizard.navigation')

    <div class="border-t border-[#a7d7f9] pt-2">
        <div class="space-y-4">
            <h2 class="text-lg">{{ __('messages.museumdataforwikipedia_subtitle') }}</h2>
            <h3 class="text-lg text-center">{{ $museum }}</h3>
            <label for="history">{{__('messages.museumdataforwikipedia_history')}}:</label>
            <textarea name="history" id="history" wire:model="history" rows="5" cols="100%"></textarea><br>
            <label for="institutionAssets">{{__('messages.museumdataforwikipedia_collection')}}:</label><br>
            <textarea name="institutionAssets" id="institutionAssets" wire:model="institutionAssets" rows="5" cols="100%"></textarea><br>
            <label for="bibliography">{{__('messages.museumdataforwikipedia_bibliography')}}:</label><br>
            <textarea name="bibliography" id="bibliography" wire:model="bibliography" rows="5" cols="100%"></textarea><br>
            <p>
                {!! __('messages.next_license') !!}
            </p>

            <x-button wire:click="submit" label="{{ __('messages.continue') }}"></x-button>
            <x-button wire:click="nextStep" label="{{ __('messages.jump') }}"></x-button>
        </div>
    </div>

</div>
