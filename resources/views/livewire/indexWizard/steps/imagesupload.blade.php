<div>
    @include('livewire.indexWizard.navigation')

    <div class="border-t border-[#a7d7f9] pt-2 spokeperson">
        <div class="space-y-4">
            <h2 class="text-lg">{{ __('messages.imagesupload_subtitle') }}</h2>
            <h3 class="text-lg text-center">{{ $museum }}</h3>

            <p>{{ __('messages.imageasupload_text.1') }}</p>
            <p>{!! __('messages.imageasupload_text.2') !!}</p>
            <p>{{ __('messages.imageasupload_text.3') }}</p>

            <label for="images">{{ __('messages.imagesupload_select') }}<span class="text-red-600 text-xs relative -top-2">*</span>:</label> <input type="file" name="images" id="images" wire:model="images" multiple><br>
            <div wire:loading wire:target="images">{{ __('messages.upload') }}...</div>
            <div>
                @if ($errors->any())
                    <span class="block error">{{ __('messages.error_image_upload') }}</span>
                    @error('images.*')<span class="block error">{{ $message }}</span>@enderror
                @endif
            </div>

            <p><b>{!! __('messages.imageasupload_text.4') !!}</b></p>

            <x-button wire:click="submit" label="{{ __('messages.continue') }}"></x-button>
            <x-button wire:click="nextStep" label="{{ __('messages.jump') }}"></x-button>
        </div>
    </div>

</div>
