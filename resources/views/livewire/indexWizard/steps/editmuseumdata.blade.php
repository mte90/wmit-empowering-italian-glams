<div>
    @include('livewire.indexWizard.navigation')

    <div class="border-t border-[#a7d7f9] pt-2">
        <div class="space-y-4">
            <h2 class="text-lg">{{ __('messages.editmuseumdata_subtitle') }}</h2>
            <h3 class="text-lg text-center">{{ $museum }}</h3>

            <div>
            @if ($errors->any())
                <span class="block error">
                {{__('messages.error_fields')}}
                </span>
            @endif
            </div>

            <label for="description">{{__('messages.museum_description')}}<span class="text-red-600 text-xs relative -top-2">*</span></label><br>
            <textarea name="description" id="description" wire:model="description" rows="5" cols="100%"></textarea><br>
            <h2 class="text-lg">{{__('messages.museum_typologies')}}</h2>
            <div class="museumslist">
                @foreach($typologies as $key => $item)
                    <input type="checkbox" wire:model="typology" value="{{$key}}" id="typology['{{$key}}']"/>
                    <label for="typology['{{$key}}']">{{$item}}</label><br>
                @endforeach
            </div>
            <label for="website">{{__('messages.website')}}<span class="text-red-600 text-xs relative -top-2">*</span>:</label>
            <input type="text" wire:model="website" name="website"><br>
            <label for="owner">{{__('messages.owner')}}:</label>
            <input type="text" wire:model="owner" name="owner"><br>
            <label for="address">{{__('messages.address')}}<span class="text-red-600 text-xs relative -top-2">*</span>:</label>
            <input type="text" wire:model="address" name="address"><br>
            <label for="manager">{{__('messages.manager')}}:</label>
            <input type="text" wire:model="manager" name="manager"><br>
            <p>
                {!! __('messages.next_license') !!}
            </p>

            <x-button wire:click="submit" label="{{ __('messages.continue') }}"></x-button>
            <x-button wire:click="redirectSurvey" label="{{ __('messages.jump') }}"></x-button>
        </div>
    </div>

</div>
