<div>
    @include('livewire.indexWizard.navigation')

    <div class="border-t border-[#a7d7f9] pt-2 spokeperson">
        <div class="space-y-4">
            <h2 class="text-lg">{{ __('messages.application_subtitle') }}</h2>
            <h3 class="text-lg text-center">{{ $museum }}</h3>
            <p class="!my-0 font-bold">{{ __('messages.application_email') . ' ' . $email }}</p>
            <p class="!my-0">{!! __('messages.application_note') !!}</p>

            <div>
            @if ($errors->any())
                <span class="block error">
                {{__('messages.error_fields')}}
                </span>
            @endif
            </div>

            <label for="letter">{{__('messages.application_upload')}}:</label><input type="file" name="letter" id="letter" wire:model="letter"><br>

            <p>{!! __('messages.application_paragraph') !!}</p>
            <p>{{ __('messages.application_paragraph_2') }}</p>

            <x-button wire:click="submit" label="{{ __('messages.continue') }}"></x-button>
            <x-button wire:click="redirectSurvey" label="{{ __('messages.application_no_thanks') }}"></x-button>
        </div>
    </div>

</div>
