<div>
    @include('livewire.indexWizard.navigation')

    <div class="border-t border-[#a7d7f9] pt-2">
        <div class="space-y-4">
            <h2 class="text-lg">{{ __('messages.finish_subtitle') }}</h2>
            <b>{{ __('messages.finish_title_list', [ 'museumname' => $museum ]) }}</b>

            {!! $status['error'] !!}

            <ul class="list-disc list-inside">
                <li>{!! $status['spokeperson'] . __('messages.finish_list.1') !!}</li>
                <li>{!! $status['application'] . __('messages.finish_list.2') !!}</li>
                <li>{!! $status['editmuseumdata'] . __('messages.finish_list.3') !!}</li>
                <li>{!! $status['museumdataforwikipedia'] . __('messages.finish_list.4') !!}</li>
                <li>{!! $status['imageauthorization'] . __('messages.finish_list.5') !!}</li>
                <li>{!! $status['imagesupload'] . __('messages.finish_list.6') !!}</li>
                <li>{!! $status['notification'] !!}</li>
            </ul>
            <p>
                {{ __('messages.finish_conclusion') }}
            </p>
        </div>
    </div>

</div>
