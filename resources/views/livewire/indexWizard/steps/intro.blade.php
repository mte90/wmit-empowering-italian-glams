<div>
    @include('livewire.indexWizard.navigation')

    <div class="border-t border-[#a7d7f9] pt-2">
        <div class="space-y-4">
            <h2 class="text-lg">{{ __('messages.intro_subtitle') }}</h2>
            <b>{{ __('messages.intro_title_list') }}</b>
            <ul class="list-disc list-inside">
                <li>{{ __('messages.intro_list.1') }}</li>
                <li>{{ __('messages.intro_list.2') }}</li>
                <li>{{ __('messages.intro_list.3') }}</li>
                <li>{{ __('messages.intro_list.4') }}</li>
                <li>{{ __('messages.intro_list.5') }}</li>
                <li>{{ __('messages.intro_list.6') }}</li>
            </ul>
            <p>
                {{ __('messages.intro_after_list') }}
            </p>
            <p>
                {{ __('messages.intro_conclusion') }}
            </p>

            <x-button wire:click="nextStep" label="{{ __('messages.continue') }}"></x-button>
        </div>
    </div>

</div>
