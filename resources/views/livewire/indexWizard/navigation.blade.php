<nav class="flex space-x-2 mb-2" aria-label="Tabs">
    @foreach($steps as $step)
        <div class="border-transparent text-gray-500 group hover:text-underline inline-flex items-center px-1 font-medium text-sm
        {{ $step->isPrevious() ? 'cursor-pointer group-hover:text-underline' : 'cursor-not-allowed' }}
        {{ $step->isCurrent() ? 'text-[#000] cursor-progress' : 'text-[#0645ad]' }}"
            @if ($step->isPrevious())
                wire:click="{{ $step->show() }}"
            @endif
        >
            <span>{{ $step->label }}</span>&nbsp;{!! str_replace( 'text-xl', 'font-medium', $step->tab_status[ $step->alias ] ) !!}
        </div>
    @endforeach
</nav>
