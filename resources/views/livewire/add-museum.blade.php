<x-modal form-action="update">
    <x-slot name="title">
        {{ __('messages.add_museum') }}
    </x-slot>

    <x-slot name="content">
        <div class="add-museum-modal">

            <div>
            @if ($errors->any())
                <span class="block error">
                {{__('messages.error_fields')}}
                </span>
            @endif
            @error('alreadyExists') <h3 class="error text-center font-semibold">{!! $message !!}</h3> @enderror
            </div>

            <label for="name">{{__('messages.museum_title')}}<span class="text-red-600 text-xs relative -top-2">*</span>:</label><input type="text" wire:model="name" name="name" size="60"><br>
            <label for="address">{{__('messages.address')}}<span class="text-red-600 text-xs relative -top-2">*</span>:</label><input type="text" wire:model="address" name="address"><br>
            <label for="email">{{__('messages.email')}}<span class="text-red-600 text-xs relative -top-2">*</span>:</label><input type="email" wire:model="email" name="email"><br>
            <label for="website">{{__('messages.website')}}:</label><input type="text" wire:model="website" name="website"><br>
            <label for="description">{{__('messages.museum_description')}}:</label><br>
            <textarea wire:model="description" name="description" rows="5" cols="90%"></textarea>
            <h2 class="text-lg">{{__('messages.museum_typologies')}}</h2>
            <div class="museumslist">
                @foreach($typologies as $key => $item)
                    <input type="checkbox" wire:model="typology" value="{{$key}}" id="typology['{{$key}}']"/>
                    <label for="typology['{{$key}}']">{{$item}}</label><br>
                @endforeach
            </div>
        </div>
    </x-slot>

    <x-slot name="buttons">
        <x-button wire:submit.prevent="update" label="{{ __('messages.add') }}"></x-button>
    </x-slot>
</x-modal>
