<x-modal>
    <x-slot name="title">
        {{ __('messages.survey_title') }}
    </x-slot>

    <x-slot name="content">
        <div class="add-museum-modal">
            {!! __('messages.why_continue', ['surveylink' => $agreesurveyURL]) !!}
        </div>
    </x-slot>

    <x-slot name="buttons">
        <x-button wire:click="goToSurvey" label="{{ __('messages.continue_to_survey') }}"></x-button>&nbsp;
        <x-button wire:click="goNext" label="{{ __('messages.continue_without_survey') }}"></x-button>
    </x-slot>
</x-modal>
