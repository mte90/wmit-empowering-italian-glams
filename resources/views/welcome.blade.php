<x-layout>
    @php
        $initialState = [
            'museum' => []
        ];

        if (!is_null($museum) && !empty($museum)) {
            if ($museum[0] === 'Q') {
                $initialState['museum']['museumPicked'] = $museum;
            } else {
                $initialState['museum']['newMuseumPicked'] = $museum;
            }
        }

        if (!$museumExists && !empty($museum)) {
            $step = '';
        }
    @endphp
    @if (!empty($step))
        <livewire:index-wizard show-step="{{$step}}" :initial-state="$initialState"/>
    @else
        <livewire:index-wizard />
    @endif
</x-layout>
