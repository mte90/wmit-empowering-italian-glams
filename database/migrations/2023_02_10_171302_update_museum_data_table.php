<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('museum_data', function (Blueprint $table) {
            $table->longText('history')->change();
            $table->longText('institutionAssets')->change();
            $table->longText('bibliography')->change();
            $table->longText('description')->change();
            $table->string('email', 100)->change();
        });

        Schema::table('spokepeople', function (Blueprint $table) {
            $table->string('email', 100)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
};
