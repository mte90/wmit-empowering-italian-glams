<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('museum_data', function (Blueprint $table) {
            $table->dropColumn('openingDate');
            $table->dropColumn('foundationDate');
            $table->string('authorizationLetter');
            $table->string('policyLetter');
            $table->string('history');
            $table->string('institutionAssets');
            $table->string('bibliography');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('museum_data');
    }
};
