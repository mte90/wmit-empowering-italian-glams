<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('museum_data', function (Blueprint $table) {
            $table->longText('museumID')->nullable()->change();
            $table->string('website')->nullable()->change();
            $table->longText('owner')->nullable()->change();
            $table->longText('manager')->nullable()->change();
            $table->longText('description')->nullable()->change();
            $table->longText('history')->nullable()->change();
            $table->longText('institutionAssets')->nullable()->change();
            $table->longText('bibliography')->nullable()->change();
            $table->string('email')->nullable()->change();
            $table->string('municipality')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
};
