<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('museum_data', function (Blueprint $table) {
            $table->id();
            $table->string('museumID');
            $table->string('name', 120);
            $table->string('address', 200);
            $table->string('email', 30);
            $table->string('website', 100);
            $table->string('municipality', 100);
            $table->string('types',1500);
            $table->string('owner');
            $table->string('manager');
            $table->string('openingDate');
            $table->string('foundationDate');
            $table->string('letter');
            $table->string('description',1500);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('museum_data');
    }
};
