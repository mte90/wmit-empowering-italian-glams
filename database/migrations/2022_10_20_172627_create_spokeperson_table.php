<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spokepeople', function (Blueprint $table) {
            $table->id();
            $table->string('museumID', 40);
            $table->string('name', 40);
            $table->string('surname', 40);
            $table->string('role', 40);
            $table->string('email', 30);
            $table->longText('note');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spokeperson');
    }
};
