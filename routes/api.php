<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Submission;
use App\AddedMuseums;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('submission', function (Request $request) {
    return Submission::create($request->all);
});

Route::post('addMuseum', function (Request $request) {
    return AddedMuseums::create($request->all);
});

Route::get('submission', 'SubmissionController@index');
Route::put('submission/{id}', 'SubmissionController@show');
