<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Livewire\Livewire;
use App\Models\MuseumData;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function (Request $request) {
    $step = $request->get('step');

    // Resume feature
    $museum = $request->get('museum');
    if (!empty($museum) && $step === 'resume') {
        if (MuseumData::museumExists($request)) {
            $wikidata_id = MuseumData::query()
            ->distinct()->select('museumID')
            ->where('id', $museum)
            ->get()->toArray();

            if (!empty($wikidata_id[0]['museumID'])) {
                return redirect()->to(URL::to('/') . '?step=resume&museum=' . $wikidata_id[0]['museumID']);
            }

            $museumdata = MuseumData::query()
            ->distinct()->select('*')
            ->where('museumID', '=', $museum)
            ->orWhere('id', '=', $museum)
            ->get()->toArray()[0];
            $status = MuseumData::wizardFinished($museumdata);
            foreach ($status as $index => $_step) {
                if (!is_bool($_step)) {
                    $step = $index;
                    break;
                }
            }

            if ($status['total'] === 6) {
                $step = 'finish';
            }
        }
    }

    // Disable notification
    if (!empty($museum) && $step === 'notification') {
        if (MuseumData::museumExists($request)) {
            MuseumData::where('museumID', '=', $museum)
            ->orWhere('id', '=', $museum)->update([
                'notification' => false,
            ]);
            $step = 'finish';
        }
    }

    // Go back after LimeSurvey
    if (!empty($museum) && $step === 'back-survey') {
        if (MuseumData::museumExists($request)) {
            MuseumData::where('museumID', '=', $museum)
            ->orWhere('id', '=', $museum)->update([
                'survey' => true,
            ]);
            $step = 'editmuseumdata';
        }
    }

    // We want to check if the step exist to avoid a crash
    try {
        Livewire::getClass($step);
    } catch (\Throwable $th) {
        $step = '';
    }

    return view('welcome')->with([
        'step' => $step,
        'museum' => MuseumData::getMuseum($request),
        'museumExists' => MuseumData::museumExists($request),
    ]);
});
