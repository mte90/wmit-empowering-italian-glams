module.exports = {
    content: [
        "./resources/views/**/*.blade.php",
        "./app/Http/Livewire/Steps/*.php",
        "./app/Models/*.php",
        "./resources/**/*.js",
        './storage/framework/views/*.php',
    ],
    options: {
      safelist: [
        'sm:max-w-2xl'
      ]
    },
    theme: {
        extend: {},
    },
    plugins: [
        require('@tailwindcss/forms'),
    ],
}
