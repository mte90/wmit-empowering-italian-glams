# Empowering Italian GLAMs

## Installation

`laravel` is just a placeholder for the folder generate by the git command or the database.

### Mysql

```
mysql> CREATE DATABASE laravel;
mysql> GRANT ALL ON laravel.* to 'laravel'@'localhost' IDENTIFIED BY 'secret_password';
mysql> FLUSH PRIVILEGES;
mysql> quit
```

### Commands to configure Laravel and php packages 

```
apt install php php-mcrypt php-gd php-mbstring php-xml composer npm
apt install apache2 libapache2-mod-php mysql-server php-mysql

chown -R www-data:www-data /var/www/laravel
chmod -R 755 /var/www/laravel
chmod -R 777 /var/www/laravel/storage

mv .env.example .env
composer install --no-dev
mkdir storage/app/authorizationLetter
mkdir storage/app/letters
mkdir storage/app/images
# npm install
# npm run build only for development
php artisan key:generate
php artisan migrate
php artisan livewire:publish
php artisan storage:link
```

Mysql commands to enable admin:

```
INSERT INTO `admin_roles` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1,	'Administrator',	'administrator',	'2023-02-01 15:53:48',	'2023-02-01 15:53:48');
INSERT INTO `admin_role_menu` (`role_id`, `menu_id`, `created_at`, `updated_at`) VALUES
(1,	2,	NULL,	NULL);
INSERT INTO `admin_permissions` (`id`, `name`, `slug`, `http_method`, `http_path`, `created_at`, `updated_at`) VALUES
(1,	'All permission',	'*',	'',	'*',	NULL,	NULL),
(2,	'Dashboard',	'dashboard',	'GET',	'/',	NULL,	NULL),
(3,	'Login',	'auth.login',	'',	'/auth/login\r\n/auth/logout',	NULL,	NULL),
(4,	'User setting',	'auth.setting',	'GET,PUT',	'/auth/setting',	NULL,	NULL),
(5,	'Auth management',	'auth.management',	'',	'/auth/roles\r\n/auth/permissions\r\n/auth/menu\r\n/auth/logs',	NULL,	NULL);
INSERT INTO `admin_role_permissions` (`role_id`, `permission_id`, `created_at`, `updated_at`) VALUES
(1,	1,	NULL,	NULL);
INSERT INTO `admin_role_users` (`role_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1,	1,	NULL,	NULL);
INSERT INTO `admin_menu` (`id`, `parent_id`, `order`, `title`, `icon`, `uri`, `permission`, `created_at`, `updated_at`) VALUES
(2,	0,	2,	'Admin',	'fa-tasks',	'',	NULL,	NULL,	NULL),
(3,	2,	3,	'Users',	'fa-users',	'auth/users',	NULL,	NULL,	NULL),
(4,	2,	4,	'Roles',	'fa-user',	'auth/roles',	NULL,	NULL,	NULL),
(5,	2,	5,	'Permission',	'fa-ban',	'auth/permissions',	NULL,	NULL,	NULL),
(6,	2,	6,	'Menu',	'fa-bars',	'auth/menu',	NULL,	NULL,	NULL),
(7,	2,	7,	'Operation log',	'fa-history',	'auth/logs',	NULL,	NULL,	NULL);
```

Let's go back with setting Laravel:

```
php artisan admin:publish
php artisan admin:create-user # generate the admin user for the application
```

On `.env` set database connection data, `APP_URL` for the domain, `APP_DEBUG` to disable (for pdocution) and the SMTP to send the email notifications.

### Apache

```
vim /etc/apache2/sites-available/000-default.conf
```

**It is required the AllowOverride rule as Laravel needs his own .htaccess configuration file**

```
<VirtualHost *:80>

        ServerAdmin webmaster@localhost
        DocumentRoot /var/www/laravel/public

        <Directory />
                Options FollowSymLinks
                AllowOverride None
        </Directory>
        <Directory /var/www/laravel>
                AllowOverride All
        </Directory>

        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined

</VirtualHost>
```

### config

#### Survey

```
'redirect_survey' => 'https://www.wikimedia.it/',
```

Set the redirect to a Livesurvey that will be prefilled with values.  
A working example on this moment of writing `https://survey.wikimedia.it/index.php/324835/newtest/Y?324835X287X4098={nomeistituzione}&324835X287X4104={nomereferente}&324835X287X4105={ruoloreferente}&324835X287X4099={emailreferente}&324835X287X4103=AO0{adesione}`.  
The LiveSurvey URL parameter are found using the browser from the IDs of the various elements in the HTML page. In this way the related fields will be already filled.  
Also this require that the Limesurvey resend the user back to the portal, for this you need to use the http://127.0.0.1:8000/?step=back-survey&museum=<museumid> and not the resume endpoint, as this one saves the status on the database. Also the URL generated requires that the is replaced automatically with the one by the user as that data is passed back.

### Cron

Notifications are automatic by emails with the command `php artisan email:wizard`.  
Automatically takes all the museum in the last 4 months that doesn't fulfilled all the data requested.  
So probably a notification once a week is good enough.

`40 8 * * 1 cd /var/www/the-folder && php artisan email:wizard`

## Notes

* `database/*.sql` includes the 3 tables with the Italian museums list
* `database/italia.sql` it is a list of province/region/city based on ISTAT data
* The software uses 2 different IDs, one is the WikiData ID that is saved in a dedicated field or the row ID (for custom manually added museum). In every query check for both the data to find the right museum as the WikiData ID starts with the `Q` letter.
* Laravel-Admin is used to generate the admin interface, also if configured it is required on a new installation to add manually a new menu item to the museum page from the UI itself to https://example.org/admin/museum from https://example.org/admin/auth/menu page

```
SELECT DISTINCT ?item ?itemLabel ?amministrativa ?amministrativaLabel WHERE {
  { ?item wdt:P17 wd:Q38. }
  { ?item wdt:P31 wd:Q33506. }
  UNION
  { ?item (wdt:P31/wdt:P279) wd:Q33506. }
  UNION
  { ?item (wdt:P31/wdt:P279/wdt:P279) wd:Q33506. }
  UNION
  { ?item (wdt:P31/wdt:P279/wdt:P279/wdt:P279) wd:Q33506. }
  SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],it". }
  ?item wdt:P131 ?amministrativa.
}
```

To generate the list of museums with a Wikidata query.

### URL prefilled

* http://127.0.0.1:8000/?step=museum&region=abruzzo&province=L%27Aquila&municipality=L%27Aquila it is possible to autofill the second wizard page
* http://127.0.0.1:8000/?step=spokeperson&museum=Q2886989 (or the id of the new museum added) it is possible to jump the third wizard page
* http://127.0.0.1:8000/?step=application&museum=Q2886989 as above
* http://127.0.0.1:8000/?step=editmuseumdata&museum=Q2886989 
* http://127.0.0.1:8000/?step=museumdataforwikipedia&museum=Q2886989 
* http://127.0.0.1:8000/?step=imageauthorization&museum=Q2886989
* http://127.0.0.1:8000/?step=imagesupload&museum=Q2886989
* http://127.0.0.1:8000/?step=finish&museum=Q2886989
* http://127.0.0.1:8000/admin to manage the data submitted
* http://127.0.0.1:8000/?step=resume&museum=Q2886989 to resume from the last step missed
* http://127.0.0.1:8000/?step=notification&museum=Q2886989 disable notification for that museum
* http://127.0.0.1:8000/?step=back-survey&museum=Q2886989 save to that museum the survey filling
